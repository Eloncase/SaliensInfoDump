﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace SaliensDump
{
    class Program
    {
        public class Planet
        {
            [JsonProperty("id")]
            public string Id { get; internal set; }
            [JsonProperty("state")]
            public PlanetState State { get; internal set; }
            [JsonProperty("giveaway_apps")]
            public List<int> Apps { get; internal set; }
        }

        public class PlanetState
        {
            [JsonProperty("name")]
            public string Name { get; internal set; }
            [JsonProperty("image_filename")]
            public string ImageFilename { get; internal set; }
            [JsonProperty("map_filename")]
            public string MapFilename { get; internal set; }
            [JsonProperty("land_filename")]
            public string LandFilename { get; internal set; }
            [JsonProperty("difficulty")]
            public int Difficulty { get; internal set; }
            [JsonProperty("giveaway_id")]
            public long GiveawayId { get; internal set; }
            [JsonProperty("active")]
            public bool Active { get; internal set; }
            [JsonProperty("activation_time")]
            public long ActivationTime { get; internal set; }
            [JsonProperty("captured")]
            public bool Captured { get; internal set; }
            [JsonProperty("capture_time")]
            public long CaptureTime { get; internal set; }
            [JsonProperty("capture_progress")]
            public double CaptureProgress { get; internal set; }
            [JsonProperty("total_joins")]
            public int TotalJoins { get; internal set; }
            [JsonProperty("current_players")]
            public int CurrentPlayers { get; internal set; }
        }

        static void Main(string[] args)
        {
            var wc = new WebClient();
            var doc = System.IO.File.ReadAllText("GetPlanets.json");

            var planetList = JsonConvert.DeserializeObject<JObject>(doc)["response"]["planets"].ToObject<List<Planet>>();

            System.IO.Directory.CreateDirectory("Images");
            System.IO.Directory.CreateDirectory(@"Images\Planets");
            System.IO.Directory.CreateDirectory(@"Images\Maps");
            System.IO.Directory.CreateDirectory(@"Images\Backgrounds");

            var i = 1;
            foreach (var pl in planetList)
            {
                wc.DownloadFile($"https://steamcdn-a.akamaihd.net/steamcommunity/public/assets/saliengame/planets/{pl.State.ImageFilename}", $@"Images\Planets\{pl.State.ImageFilename}");
                wc.DownloadFile($"https://steamcdn-a.akamaihd.net/steamcommunity/public/assets/saliengame/maps/{pl.State.MapFilename}", $@"Images\Maps\{pl.State.MapFilename}");
                wc.DownloadFile($"https://steamcdn-a.akamaihd.net/steamcommunity/public/assets/saliengame/backgrounds/{pl.State.LandFilename}", $@"Images\Backgrounds\{pl.State.LandFilename}");

                Console.WriteLine($"{i++} / {planetList.Count}");
            }
        }
    }
}
